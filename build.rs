use built;

fn main() {
    // as in write_built_file, but we want other options
    let src = std::env::var("CARGO_MANIFEST_DIR").unwrap();
    let dst = std::path::Path::new(&std::env::var("OUT_DIR").unwrap()).join("built.rs");

    if std::path::Path::new(&src).join("Cargo.lock").exists() {
        built::write_built_file_with_opts(&built::Options::default().set_dependencies(true), src.as_ref(), &dst)
            .expect("Failed to acquire build-time information");
    } else {
        // The above doesn't work during `cargo publish`. Not sure yet which other situations that
        // will trigger in, but this allows it to build with just minimally degraded functionality.
        //
        // See-Also: https://github.com/lukaslueg/built/issues/32

        built::write_built_file_with_opts(&built::Options::default(), src.as_ref(), &dst)
            .expect("Failed to acquire build-time information");
        use std::io::Write;
        let mut dstfile = std::fs::OpenOptions::new().append(true).open(dst).unwrap();
        dstfile.write(b"pub const DEPENDENCIES: [(&str, &str); 0] = [];\n").unwrap();
    }
}
