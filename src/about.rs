//! A plain message displaying the version of the software and possibly further information and
//! links

#[allow(dead_code)]
mod built_info {
    include!(concat!(env!("OUT_DIR"), "/built.rs"));
}

use yew::prelude::*;

pub struct About;

impl Component for About {
    type Message = ();
    type Properties = ();

    fn create(_: Self::Properties, _: ComponentLink<Self>) -> Self {
        About
    }

    fn update(&mut self, _: Self::Message) -> ShouldRender {
        false
    }

    fn change(&mut self, _: Self::Properties) -> ShouldRender {
        false
    }

    fn view(&self) -> Html {
        let readme = yew::utils::document().create_element("details").unwrap();
        let readme_text = include_str!("../README.md");
        // FIXME: Just because this is not const, the binary size roughly doubles (0.6 to 1.8MiB in
        // release, 5 to 10 MiB in debug).
        let mut options: comrak::ComrakOptions = Default::default();
        options.extension.footnotes = true;
        let readme_html = comrak::markdown_to_html(readme_text, &options);
        readme.set_inner_html(&readme_html);

        html! { <>
            <h1> { "Verdigirs CoAP demo application" }</h1>
            <p> { built_info::PKG_DESCRIPTION } </p>
            <p> { format!("Created by {}. Licensed under terms of {}. ", built_info::PKG_AUTHORS, built_info::PKG_LICENSE) } <a href=built_info::PKG_REPOSITORY> { "Source code" } </a> { " is available." }</p>
            <h2> { "Project README" } </h2>
            { Html::VRef(readme.into()) }
            <h2> { "Build information" } </h2>
            <p> { if let (Some(git), Some(dirty)) = (built_info::GIT_VERSION, built_info::GIT_DIRTY) {
                format!("{} version {} (from git version {} ({}))", built_info::PKG_NAME, built_info::PKG_VERSION, git, if dirty { "dirty" } else { "clean" }) 
            } else {
                format!("{} version {}", built_info::PKG_NAME, built_info::PKG_VERSION) 
            } }
            { " (built " }
            { built_info::CI_PLATFORM.map(|p| format!("on {}, ", p)).unwrap_or_else(|| "".to_string()) }
            { format!("with {})", built_info::RUSTC_VERSION) }
            </p>
            <details id="deps">
            <summary> { "Used crates" } </summary>
            <ul>
                { for built_info::DEPENDENCIES.iter().map(|(cratename, version)| html! { <li> <a href=format!("https://crates.io/crates/{}", cratename)> { cratename } </a> { " " } { version } </li> }) }
                // no build-dependencies in built_info
                <li> { "… and " } <a href="https://crates.io/crates/built"> { "built" } </a> { ", which neatly provides this list" } </li>
            </ul>
            </details>
        </> }
    }
}
