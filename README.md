Verdigris is a demo of a CoAP GUI run in the browser communicating via WebSockets.

It aims to become usable as a generic CoAP client
(possibly with some limited server functionality)
that can be used from the browser for exploration, leaning and debugging.

Eventually it should be usable as a replacement for the venerable [Copper] browser plug-in
that got overtaken by the browser plugin APIs.
Unlike Copper, this runs purely as a website, and can not talk CoAP-over-UDP.
Instead, it relies on a public or local cross proxy that is accessible using CoAP-over-WS,
or possibly other browser-accessible transports.

[Copper]: https://github.com/mkovatsc/Copper

Currently, it provides three distinct modes of operation:

* In message exchanger mode, it can be used to generate CoAP requests by assembling them from options
  (or useing some of the preset ones), and shows any incoming responses in decoded form.

* In color server mode, it provides a monochromous area whose color can be changed by CoAP clients.
  it registers at a Resource Directory[^1] with a given (initially random) endpoint name
  and asks to be reverse-proxied behind a globally reachable name.

  The server also runs (but announces incompletely) the demo applications from [coap-message-demos].
  For example, it offers a resource `/message/info` to which messages can be POSTed;
  their content shows up in the browser console through the `log` crate.

  [coap-message-demos]: https://gitlab.com/chrysn/coap-message-demos

* In color client mode, it connects to a Resource Directory[^1] via a proxy,
  looking for compatible resurces at a color server.

  All discovered resources can be set (together or individually) to a selected color
  through the same proxy.

* In the highly experimental Bluetooth Low Energy GATT mode,
  a device speaking [CoAP-over-GATT](https://www.ietf.org/archive/id/draft-amsuess-core-coap-over-gatt-01.html)
  can be accessed from a Chromium-based browser,
  and made available via the Resource Directory.

[^1]: At startup, it connects to the one at <https://rd.coap.amsuess.com/>.

Run and host using `cargo build --target wasm32-unknown-unknown --release && wasm-bindgen target/wasm32-unknown-unknown/release/verdigris.wasm --out-dir public/ --web && python3 -m http.server -d ./public 8080`,
and go to <https://localhost:8080/>.
The Python part can be replaced with any static web server.
Replacing the direct use of [wasm-bindgen CLI]
with [trunk] or [wasm-pack] only needs minor adjustments in the provided `index.html` file;
see the project's history for some variations that have been used in the past.

[wasm-bindgen CLI]: https://crates.io/crates/wasm-bindgen-cli
[trunk]: https://crates.io/crates/trunk
[wasm-pack]: https://crates.io/crates/wasm-pack

The latest version is available online at <https://chrysn.gitlab.io/verdigris/>.
